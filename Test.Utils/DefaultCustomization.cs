﻿using AutoFixture;
using AutoFixture.AutoMoq;

namespace Test.Utils
{
    public class DefaultCustomization : CompositeCustomization
    {
        public DefaultCustomization()
            : base(new AutoMoqCustomization())
        { }
    }
}
