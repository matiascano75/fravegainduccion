﻿namespace Domain.Entities
{
    public class Promocion
    {
        public Guid Id { get; private set; }
        public IEnumerable<string> MediosDePago { get; private set; }
        public IEnumerable<string> Bancos { get; private set; }
        public IEnumerable<string> CategoriasProductos { get; private set; }
        public int? MaximaCantidadDeCuotas { get; private set; }
        public decimal? ValorInteresCuotas { get; private set; }
        public decimal? PorcentajeDeDescuento { get; private set; }
        public DateTime? FechaInicio { get; private set; }
        public DateTime? FechaFin { get; private set; }
        public bool Activo { get; private set; }
        public DateTime FechaCreacion { get; private set; }
        public DateTime? FechaModificacion { get; private set; }

        public Promocion(int? maximaCantidadCuotas,
            decimal? valorInteresCuota, decimal? porcentajeDeDescuento,
             DateTime? fechaInicio, DateTime? fechaFin, IEnumerable<string> mediosDePago,
             IEnumerable<string> bancos, IEnumerable<string> categoriasProductos,
             Guid? id, bool activo = true
            )
        {
            if (id.HasValue) { this.Id = (Guid)id; } else { this.Id = Guid.NewGuid(); }
            MediosDePago = mediosDePago ?? new string[0];
            Bancos = bancos ?? new string[0];
            CategoriasProductos = categoriasProductos ?? new string[0];
            MaximaCantidadDeCuotas = maximaCantidadCuotas;
            ValorInteresCuotas = valorInteresCuota;
            PorcentajeDeDescuento = porcentajeDeDescuento;
            FechaInicio = fechaInicio;
            FechaFin = fechaFin;
            Activo = activo;
            FechaCreacion = DateTime.Now;
        }
    }
}
