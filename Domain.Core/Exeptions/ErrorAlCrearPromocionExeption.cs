﻿namespace Domain.Core.Exeptions
{
    public class ErrorAlCrearPromocionExeption : Exception
    {
        private static string mensaje = "Error al insertar promocion";
        public ErrorAlCrearPromocionExeption() : base(mensaje)
        {
        }
    }
}
