﻿namespace Domain.Core.Exeptions
{
    public class CategoriaNoExisteExeption : Exception
    {
        private static string mensaje = "Se ingreso una categoria que no existe.";

        public CategoriaNoExisteExeption() : base(mensaje)
        {

        }
    }
}
