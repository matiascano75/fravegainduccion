﻿namespace Domain.Core.Exeptions
{
    public class FechaFinYFechaInicioInvalidasExeption : Exception
    {
        private static string mensaje = "Rango de fechas inválido.";
        public FechaFinYFechaInicioInvalidasExeption() : base(mensaje)
        {
            Console.WriteLine(mensaje);
        }
    }
}
