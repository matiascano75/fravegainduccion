﻿using Microsoft.Extensions.Logging;

namespace Domain.Core.Exeptions
{
    public class CantCuotasOProcentajeDescSinValorExeption : Exception
    {
        private static string mensaje = "La cantidad de cuotas y el porcentaje no pueden estar vacios";
        public CantCuotasOProcentajeDescSinValorExeption() : base(mensaje)
        {
        }
    }
}
