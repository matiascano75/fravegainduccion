﻿namespace Domain.Core.Exeptions
{
    public class CantCuotasYPorcentajeDescConValorExeption : Exception
    {
        public CantCuotasYPorcentajeDescConValorExeption(string mensaje) : base(mensaje)
        {
            Console.WriteLine(mensaje);
        }
    }
}
