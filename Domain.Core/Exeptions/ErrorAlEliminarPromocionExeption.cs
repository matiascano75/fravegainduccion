﻿namespace Domain.Core.Exeptions
{
    public class ErrorAlEliminarPromocionExeption : Exception
    {
        private static string mensaje = "Error al eliminar promocion";
        public ErrorAlEliminarPromocionExeption() : base(mensaje)
        {
        }
    }
}
