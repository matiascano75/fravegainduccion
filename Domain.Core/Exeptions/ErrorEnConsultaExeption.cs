﻿namespace Domain.Core.Exeptions
{
    public class ErrorEnConsultaExeption : Exception
    {
        private static string mensaje = "Error al consultar base de datos";
        public ErrorEnConsultaExeption() : base(mensaje)
        {

        }
    }
}
