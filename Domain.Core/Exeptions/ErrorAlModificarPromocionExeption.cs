﻿namespace Domain.Core.Exeptions
{
    public class ErrorAlModificarPromocionExeption : Exception
    {
        private static string mensaje = "Error al modificar promocion";
        public ErrorAlModificarPromocionExeption() : base(mensaje)
        {
        }
    }
}
