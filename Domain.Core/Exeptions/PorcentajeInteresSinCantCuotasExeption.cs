﻿namespace Domain.Core.Exeptions
{
    public class PorcentajeInteresSinCantCuotasExeption : Exception
    {
        private static string mensaje = "Porcentaje de interes solo puede tener valor si se ingresa cantidad de cuotas.";
        public PorcentajeInteresSinCantCuotasExeption() : base(mensaje)
        {

        }
    }
}
