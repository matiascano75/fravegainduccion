﻿namespace Domain.Core.Exeptions
{
    public class MedioDePagoNoExisteExeption : Exception
    {
        private static string mensaje = "Se ingreso un medio de pago no válido.";

        public MedioDePagoNoExisteExeption() : base(mensaje)
        {

        }
    }
}
