﻿namespace Domain.Core.Exeptions
{
    public class BancoNoExisteExeption : Exception
    {
        private static string mensaje = "Se ingreso un banco no válido.";

        public BancoNoExisteExeption() : base(mensaje)
        {

        }
    }
}
