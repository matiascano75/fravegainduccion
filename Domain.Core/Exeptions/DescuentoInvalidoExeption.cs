﻿namespace Domain.Core.Exeptions
{
    public class DescuentoInvalidoExeption : Exception
    {
        private static string mensaje = "Rango de descuento inválido.";
        public DescuentoInvalidoExeption() : base(mensaje)
        {
            Console.WriteLine(mensaje);
        }
    }
}
