﻿namespace Domain.Core.Exeptions
{
    public class PromocionExistenteExeption : Exception
    {
        private static string mensaje = "Ya existe una promocion para estas caracteristicas.";
        public PromocionExistenteExeption() : base(mensaje)
        {
        }
    }
}
