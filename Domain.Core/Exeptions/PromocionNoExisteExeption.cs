﻿namespace Domain.Core.Exeptions
{
    public class PromocionNoExisteExeption : Exception
    {
        public PromocionNoExisteExeption(string mensaje) : base(mensaje)
        {

        }
    }
}
