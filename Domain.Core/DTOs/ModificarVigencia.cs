﻿namespace Domain.Core.DTOs
{
    public class ModificarVigencia
    {
        public Guid Id { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }

    }
}
