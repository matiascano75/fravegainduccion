﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Core.DTOs
{
    public class PromocionParaVenta
    {
        public string? MediosDePago { get; set; }
        public string? Bancos { get; set; }
        public IEnumerable<string>? CategoriasProductos { get; set; }
        
    }
}
