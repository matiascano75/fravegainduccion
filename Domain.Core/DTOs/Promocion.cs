﻿namespace Domain.Core.DTOs
{
    public class Promocion
    {
        public Guid Id { get; set; }
        public IEnumerable<string> MediosDePago { get; set; }
        public IEnumerable<string> Bancos { get; set; }
        public IEnumerable<string> CategoriasProductos { get; set; }
        public int? MaximaCantidadDeCuotas { get; set; }
        public decimal? PorcentajeDeDescuento { get; set; }
        public decimal? ValorInteresCuotas { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public bool Activo { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public Promocion(
            IEnumerable<string> mediosDePago,
            IEnumerable<string> bancos,
            IEnumerable<string> categoriasProductos,
            int? maximaCantidadCuotas,
            decimal? valorInteresCuota,
            decimal? porcentajeDeDescuento,
            DateTime? fechaInicio,
            DateTime? fechaFin,
            Guid? id,
            bool? activo 
         )
        {
            Id = id ?? Guid.NewGuid();
            MediosDePago = mediosDePago ?? new string[0];
            Bancos = bancos ?? new string[0];
            CategoriasProductos = categoriasProductos ?? new string[0];
            MaximaCantidadDeCuotas = maximaCantidadCuotas;
            ValorInteresCuotas = valorInteresCuota;
            PorcentajeDeDescuento = porcentajeDeDescuento;
            FechaInicio = fechaInicio;
            FechaFin = fechaFin;
            Activo = activo ?? true;
            FechaCreacion = DateTime.Now;
        }

        public Entities.Promocion CreateEntity()
        {
            return new Entities.Promocion(MaximaCantidadDeCuotas, ValorInteresCuotas,
                PorcentajeDeDescuento, FechaInicio,
                FechaFin, MediosDePago,
                Bancos, CategoriasProductos,
                Id);
        }


    }
}
