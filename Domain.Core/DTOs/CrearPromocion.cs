﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Core.DTOs
{
    public class CrearPromocion
    {
        public IEnumerable<string> MediosDePago { get; set; }
        public IEnumerable<string> Bancos { get; set; }
        public IEnumerable<string> CategoriasProductos { get; set; }
        public int? MaximaCantidadDeCuotas { get; set; }
        public decimal? PorcentajeDeDescuento { get; set; }
        public decimal? ValorInteresCuotas { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }

        public Promocion CreateDto()
        {
           return new Promocion(MediosDePago,Bancos,
                CategoriasProductos,MaximaCantidadDeCuotas,ValorInteresCuotas,
                PorcentajeDeDescuento,FechaInicio,FechaFin,null,null);
        }
    }
}
