﻿using Domain.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Core.Repositories
{
    public interface IParametriaExterna
    {
        public void ValidarValores(Promocion p);
        //public void ValidarCategoria(IEnumerable<string> categorias);
        //public void ValidarBanco(IEnumerable<string> bancos);
        //public void ValidarMedioDePago(IEnumerable<string> mediosDePago);
    }
}
