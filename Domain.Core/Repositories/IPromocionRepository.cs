﻿using Domain.Core.DTOs;

namespace Domain.Core.Repositories
{
    public interface IPromocionRepository
    {
        public Task<IEnumerable<Promocion>> GetAllAsync();
        public Task<Promocion> FirstOrDefaultAsync(Guid id);
        public Task<IEnumerable<Promocion>> GetAllActiveAsync();
        public Task<IEnumerable<Promocion>> GetAllActiveByDateAsync(DateTime fecha);
        public Task<IEnumerable<Promocion>> GetAllActiveForSale(PromocionParaVenta p);
        public Task<Guid> InsertOneAsync(Promocion p);
        public Task UpdateOneAsync(Promocion p);
        public Task DeleteOneAsync(Guid id);
        public Task UpdateDate(ModificarVigencia dto);
    }
}

//public class PromocionService : IPromocionService
//{
//    private IEnumerable<Promocion> _promocion;
//    public PromocionService(IOptions<MongoDbSettings> options)
//    {
//        var cliente = new MongoClient(options.Value.ConnectionString);
//        var database = cliente.GetDatabase(options.Value.DatabaseName);
//        _promocion = database.GetCollection<Promocion>(options.Value.Collection).AsQueryable<Promocion>();
//    }

//    public IEnumerable<Promocion> GetAll()
//    {
//        return _promocion;
//    }
//    public async Task<Promocion> FirstOrDefaultAsync(Guid id)
//    {
//        return await Task.Run(() => _promocion.FirstOrDefault((x) => x.Id == id));
//    }
//}

