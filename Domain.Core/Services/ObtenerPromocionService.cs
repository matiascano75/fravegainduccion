﻿using Domain.Core.DTOs;
using Domain.Core.Repositories;
using Domain.Core.Validators;

namespace Domain.Core.Services
{
    public interface IObtenerPromocionService
    {
        public Task<Promocion> ObtenerPromocionAsync(Guid guid);
    }
    public class ObtenerPromocionService : IObtenerPromocionService
    {
        private readonly IPromocionRepository _PromocionRepository;
        public ObtenerPromocionService(IPromocionRepository repository)
        {
            _PromocionRepository = repository ?? throw new ArgumentNullException(nameof(repository));
        }
        public async Task<Promocion> ObtenerPromocionAsync(Guid guid)
        {
            return await _PromocionRepository.FirstOrDefaultAsync(guid);
        }
    }
}
