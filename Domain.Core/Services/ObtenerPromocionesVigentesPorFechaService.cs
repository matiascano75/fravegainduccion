﻿using Domain.Core.DTOs;
using Domain.Core.Repositories;
using Domain.Core.Validators;

namespace Domain.Core.Services
{
    public interface IObtenerPromocionesVigentesPorFechaService
    {
        public Task<IEnumerable<Promocion>> ObtenerPromocionesVigentesPorFechaAsync(DateTime fecha);
    }

    public class ObtenerPromocionesVigentesPorFechaService : IObtenerPromocionesVigentesPorFechaService
    {
        private readonly IPromocionRepository _PromocionRepository;
        private IValidarPromocion ValidarPromocion;
        public ObtenerPromocionesVigentesPorFechaService(IPromocionRepository repository, IValidarPromocion validarPromocion)
        {
            _PromocionRepository = repository ?? throw new ArgumentNullException(nameof(repository));
            ValidarPromocion = validarPromocion ?? throw new ArgumentNullException(nameof(validarPromocion));
        }

        public async Task<IEnumerable<Promocion>> ObtenerPromocionesVigentesPorFechaAsync(DateTime fecha)
        {
            return await _PromocionRepository.GetAllActiveByDateAsync(fecha);
        }
    }
}
