﻿using Domain.Core.Repositories;
using Domain.Core.Validators;
using Microsoft.Extensions.Logging;
using Domain.Core.DTOs;

namespace Domain.Core.Services
{
    public interface ICrearPromocionService
    {
        public Task<Guid> CrearPromocionAsync(CrearPromocion nuevaPromocion);
    }
    public class CrearPromocionService : ICrearPromocionService
    {
        private readonly IPromocionRepository _PromocionRepository;
        private IValidarPromocion ValidarPromocion;
        private IParametriaExterna ParametriaExterna;

        public CrearPromocionService(IPromocionRepository repository,
            IValidarPromocion validarPromocion,
            IParametriaExterna parametriaExterna)
        {
            _PromocionRepository = repository ?? throw new ArgumentNullException(nameof(repository));
            ValidarPromocion = validarPromocion ?? throw new ArgumentNullException(nameof(validarPromocion));
            ParametriaExterna = parametriaExterna ?? throw new ArgumentNullException(nameof(parametriaExterna));
        }

        public async Task<Guid> CrearPromocionAsync(CrearPromocion nuevaPromocion)
        {
            await ValidarPromocion.ValidarInsercionAsync(nuevaPromocion.CreateDto());
            ParametriaExterna.ValidarValores(nuevaPromocion.CreateDto());
            var result = await _PromocionRepository.InsertOneAsync(nuevaPromocion.CreateDto());
            return result;
        }
    }
}
