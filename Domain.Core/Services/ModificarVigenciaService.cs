﻿using Domain.Core.DTOs;
using Domain.Core.Exeptions;
using Domain.Core.Repositories;
using Domain.Core.Validators;


namespace Domain.Core.Services
{
    public interface IModificarVigenciaService
    {
        public Task ModificarVigencia(ModificarVigencia promocionModificada);
    }
    public class ModificarVigenciaService : IModificarVigenciaService
    {
        private readonly IPromocionRepository _PromocionRepository;
        private IValidarPromocion ValidarPromocion;
        public ModificarVigenciaService(IPromocionRepository repository, IValidarPromocion validarPromocion)
        {
            _PromocionRepository = repository ?? throw new ArgumentNullException(nameof(repository));
            ValidarPromocion = validarPromocion ?? throw new ArgumentNullException(nameof(validarPromocion));
        }

        public async Task ModificarVigencia(ModificarVigencia promocionModificada)
        {
            ValidarPromocion.FechaFinYFechaInicioInvalidas(promocionModificada.FechaInicio, promocionModificada.FechaFin);
            await _PromocionRepository.UpdateDate(promocionModificada);
        }
    }
}
