﻿using Domain.Core.Repositories;



namespace Domain.Core.Services
{
    public interface IEliminarPromocionService
    {
        public Task EliminarPromocionAsync(Guid id);
    }
    public class EliminarPromocionService : IEliminarPromocionService
    {
        private readonly IPromocionRepository _promocionRepository;

        public EliminarPromocionService(IPromocionRepository repository)
        {
            _promocionRepository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task EliminarPromocionAsync(Guid id)
        {
                await _promocionRepository.DeleteOneAsync(id);
        }
    }
}
