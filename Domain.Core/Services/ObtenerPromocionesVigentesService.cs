﻿using Domain.Core.Exeptions;
using Domain.Core.Repositories;
using Domain.Core.Validators;
using Domain.Core.DTOs;

namespace Domain.Core.Services
{
    public interface IObtenerPromocionesVigentesService
    {
        public Task<IEnumerable<Promocion>> ObtenerPromocionesVigentesAsync();
    }
    public class ObtenerPromocionesVigentesService : IObtenerPromocionesVigentesService
    {
        private readonly IPromocionRepository _PromocionRepository;
        public ObtenerPromocionesVigentesService(IPromocionRepository repository, IValidarPromocion validarPromocion)
        {
            _PromocionRepository = repository ?? throw new ArgumentNullException(nameof(repository));
        }
        public async Task<IEnumerable<Promocion>> ObtenerPromocionesVigentesAsync()
        {
            return await _PromocionRepository.GetAllActiveAsync();
        }
    }
}
