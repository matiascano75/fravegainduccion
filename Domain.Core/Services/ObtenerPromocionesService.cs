﻿using Domain.Core.Exeptions;
using Domain.Core.Validators;
using Domain.Core.Repositories;
using Domain.Core.DTOs;

namespace Domain.Core.Services
{
    public interface IObtenerPromocionesService
    {
        public Task<IEnumerable<Promocion>> ObtenerPromocionesAsync();
    }
    public class ObtenerPromocionesService : IObtenerPromocionesService
    {
        private readonly IPromocionRepository _PromocionRepository;
        public ObtenerPromocionesService(IPromocionRepository repository)
        {
            _PromocionRepository = repository ?? throw new ArgumentNullException(nameof(repository));
        }
        public async Task<IEnumerable<Promocion>> ObtenerPromocionesAsync()
        {
            return await _PromocionRepository.GetAllAsync();
        }
    }
}
