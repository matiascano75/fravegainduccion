﻿using Domain.Core.DTOs;
using Domain.Core.Repositories;
using Domain.Core.Validators;

namespace Domain.Core.Services
{
    public interface IModificarPromocionService
    {
        public Task ModificarPromocionAsync(Promocion promocionModificada);
    }
    public class ModificarPromocionService : IModificarPromocionService
    {
        // private List<Promocion> _promocion;
        private readonly IPromocionRepository _PromocionRepository;
        private IValidarPromocion ValidarPromocion;
        private IParametriaExterna ParametriaExterna;

        public ModificarPromocionService(IPromocionRepository repository,
            IValidarPromocion validarPromocion,
            IParametriaExterna parametriaExterna)
        {
            _PromocionRepository = repository ?? throw new ArgumentNullException(nameof(repository));
            ValidarPromocion = validarPromocion ?? throw new ArgumentNullException(nameof(validarPromocion));
            ParametriaExterna = parametriaExterna ?? throw new ArgumentNullException(nameof(parametriaExterna));

        }

        public async Task ModificarPromocionAsync(Promocion promocionModificada)
        {
            await ValidarPromocion.ValidarInsercionAsync(promocionModificada);
            ParametriaExterna.ValidarValores(promocionModificada);
            await _PromocionRepository.UpdateOneAsync(promocionModificada);//, updateDefinition);
        }
    }
}
