﻿using Domain.Core.DTOs;
using Domain.Core.Repositories;
using Domain.Core.Validators;

namespace Domain.Core.Services
{
    public interface IObtenerPromocionesVigentesParaVentaService
    {
        public Task<IEnumerable<Promocion>> ObtenerPromocionesVigentesParaVentaAsync(
            PromocionParaVenta p);
    }

    public class ObtenerPromocionesVigentesParaVentaService : IObtenerPromocionesVigentesParaVentaService
    {
        private readonly IPromocionRepository _PromocionRepository;
        public ObtenerPromocionesVigentesParaVentaService(
            IPromocionRepository repository)
        {
            _PromocionRepository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task<IEnumerable<Promocion>> ObtenerPromocionesVigentesParaVentaAsync(
            PromocionParaVenta p)
        {
            return await _PromocionRepository.GetAllActiveForSale(p);
        }
    }
}
