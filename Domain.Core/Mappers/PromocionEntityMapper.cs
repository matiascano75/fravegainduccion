﻿using Domain.Core.DTO;
using Domain.Entities;

namespace Domain.Core.Mappers
{
    public class PromocionEntityMapper
    {
        public static Entities.Promocion createEntity(DTO.Promocion promocion)
        {
            return new Entities.Promocion(promocion.MaximaCantidadDeCuotas, promocion.ValorInteresCuotas, promocion.PorcentajeDeDescuento,
                promocion.FechaInicio, promocion.FechaFin, promocion.MediosDePago.ToArray(), promocion.Bancos.ToArray(), promocion.CategoriasProductos.ToArray(), promocion.Id);
        }
    }
}
