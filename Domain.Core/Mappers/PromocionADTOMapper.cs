﻿using Domain.Core.DTO;
using Domain.Entities;

namespace Domain.Core.Mappers
{
    public class PromocionDTOMapper
    {
        public static DTO.Promocion createDTO(Entities.Promocion promocion)
        {
            return new DTO.Promocion
            {
                Id = promocion.Id,
                Bancos = promocion.Bancos.ToArray(),
                MaximaCantidadDeCuotas = promocion.MaximaCantidadDeCuotas,
                CategoriasProductos = promocion.CategoriasProductos.ToArray(),
                FechaFin = promocion.FechaFin,
                FechaInicio = promocion.FechaInicio,
                MediosDePago = promocion.MediosDePago.ToArray(),
                PorcentajeDeDescuento = promocion.PorcentajeDeDescuento,
                ValorInteresCuotas = promocion.ValorInteresCuotas,
                Activo = promocion.Activo,
                FechaCreacion = promocion.FechaCreacion,
                FechaModificacion = promocion.FechaModificacion
            };
        }

    }
}
