﻿using Domain.Core.DTO;
using Domain.Entities;

namespace Domain.Core.Mappers
{
    public class PromocionListADTOListMapper
    {
        public static async Task<List<DTO.Promocion>> createList(List<Entities.Promocion> promocion)
        {
            List<DTO.Promocion> lista = new List<DTO.Promocion>();
            await Task.Run(() =>
            {
                foreach (var p in promocion)
                {
                    lista.Add(PromocionDTOMapper.createDTO(p));
                }
            });
            return lista;

        }
    }
}
