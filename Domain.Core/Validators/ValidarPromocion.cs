﻿using Domain.Core.Repositories;
using Domain.Core.DTOs;
using Domain.Core.Exeptions;

namespace Domain.Core.Validators
{
    public interface IValidarPromocion
    {
        public Task ValidarInsercionAsync(Promocion nuevaPromocion);
        public Task ValidarSolapamiento(Promocion nuevaPromocion);
        public void CantidadCuotasOPorcentajeDescuentoSinValor(Promocion nuevaPromocion);
        public void PorcentajeInteresSinCantCuotas(Promocion nuevaPromocion);
        public void FechaFinYFechaInicioInvalidas(DateTime? fechaInicio, DateTime? fechaFin);
        public void PorcentajeDeDescuentoInvalido(Promocion nuevaPromocion);
        public bool ContainsAny(List<string> listaValores, List<string> listaValoresRepositorio);
    }
    public class ValidarPromocion : IValidarPromocion
    {
        private readonly IPromocionRepository _PromocionRepository;
        public ValidarPromocion(IPromocionRepository promocionRepository)
        {
            _PromocionRepository = promocionRepository ?? throw new ArgumentNullException(nameof(promocionRepository));
        }

        public async Task ValidarInsercionAsync(Promocion nuevaPromocion)
        {
            await ValidarSolapamiento(nuevaPromocion);
            CantidadCuotasOPorcentajeDescuentoSinValor(nuevaPromocion);
            PorcentajeInteresSinCantCuotas(nuevaPromocion);
            FechaFinYFechaInicioInvalidas(nuevaPromocion.FechaInicio, nuevaPromocion.FechaFin);
            PorcentajeDeDescuentoInvalido(nuevaPromocion);
        }
        public async Task ValidarSolapamiento(Promocion nuevaPromocion)
        {
            var list = await _PromocionRepository.GetAllAsync();
            foreach (Promocion promocion in list)
            {
                if ((ContainsAny(nuevaPromocion.Bancos.ToList(), promocion.Bancos.ToList()) ||
                   ContainsAny(nuevaPromocion.MediosDePago.ToList(), promocion.MediosDePago.ToList()) ||
                   ContainsAny(nuevaPromocion.CategoriasProductos.ToList(), promocion.CategoriasProductos.ToList())) &&
                   (promocion.Activo))
                {
                    throw new PromocionExistenteExeption();
                }
            }
        }
        public void CantidadCuotasOPorcentajeDescuentoSinValor(Promocion nuevaPromocion)
        {
            if (nuevaPromocion.MaximaCantidadDeCuotas == null && nuevaPromocion.PorcentajeDeDescuento == null)
            {
                throw new CantCuotasOProcentajeDescSinValorExeption();
            }
        }
        public void PorcentajeInteresSinCantCuotas(Promocion nuevaPromocion)
        {
            if (nuevaPromocion.ValorInteresCuotas != null && nuevaPromocion.MaximaCantidadDeCuotas == null)
            {
                throw new PorcentajeInteresSinCantCuotasExeption();
            }
        }
        public void FechaFinYFechaInicioInvalidas(DateTime? fechaInicio, DateTime? fechaFin)
        {
            if (fechaInicio > fechaFin)
            {
                throw new FechaFinYFechaInicioInvalidasExeption();
            }
        }
        public void PorcentajeDeDescuentoInvalido(Promocion nuevaPromocion)
        {
            if (nuevaPromocion.PorcentajeDeDescuento < 5 ||
                nuevaPromocion.PorcentajeDeDescuento > 80)
            {
                throw new DescuentoInvalidoExeption();
            }
        }
        public bool ContainsAny(List<string> listaValores, List<string> listaValoresRepositorio)
        {
            foreach (string promocion in listaValoresRepositorio)
            {
                foreach (string nuevaPromocion in listaValores)
                {
                    if (nuevaPromocion == promocion)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
