﻿using AutoMapper;
using Domain.Core.DTO;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Core.MapProfiles
{
    public class PromocionDtoProfile :Profile
    {
        public PromocionDtoProfile()
        {
            CreateMap<Promocion, PromocionDTO>();
        }
    }
}
