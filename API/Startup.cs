using API.Extensions;
using Domain.Core.Repositories;
using Domain.Core.Services;
using Domain.Core.Validators;
using Infrastructure;
using Infrastructure.ExternalServices;
using Infrastructure.Options;
using Infrastructure.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using MongoDB.Driver;
using Serilog;
using Serilog.Sinks.Elasticsearch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddOptions()
                //.AddSingleton(sp => new MongoClient(sp.GetRequiredService<IOptionsSnapshot<MongoOptions>>().Value.ConnectionString))
                .AddScoped<DataContext>()
                .AddScoped<IPromocionRepository, PromocionRepository>()
                .AddScoped<ICrearPromocionService, CrearPromocionService>()
                .AddScoped<IEliminarPromocionService, EliminarPromocionService>()
                .AddScoped<IModificarPromocionService, ModificarPromocionService>()
                .AddScoped<IModificarVigenciaService, ModificarVigenciaService>()
                .AddScoped<IObtenerPromocionesService, ObtenerPromocionesService>()
                .AddScoped<IObtenerPromocionesVigentesPorFechaService, ObtenerPromocionesVigentesPorFechaService>()
                .AddScoped<IObtenerPromocionesVigentesService, ObtenerPromocionesVigentesService>()
                .AddScoped<IObtenerPromocionService, ObtenerPromocionService>()
                .AddScoped<IObtenerPromocionesVigentesParaVentaService, ObtenerPromocionesVigentesParaVentaService>()
                .AddScoped<IValidarPromocion, ValidarPromocion>()
                .AddScoped<IParametriaExterna, ParametriaExternaService>()
                .AddScoped<IValidarParametriaExterna, ValidarParametriaExterna>()
                .AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "API v1"));
            }


            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
            app.UseErrorHandlierMiddleware();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
