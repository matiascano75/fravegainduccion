﻿using API.Middleware;
using Microsoft.AspNetCore.Builder;

namespace API.Extensions
{
    public static class AppExtensions
    {
        public static void UseErrorHandlierMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ErrorHandlerMiddlewere>();
        }
    }
}
