﻿using Domain.Core.DTOs;
using Domain.Core.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Controllers
{
    [ApiController]
    [Route("promocion")]
    public class PromocionController
    {
        private readonly ICrearPromocionService _crearPromocion;
        private readonly IEliminarPromocionService _eliminarPromocion;
        private readonly IModificarPromocionService _modificarPromocion;
        private readonly IModificarVigenciaService _modificarVigencia;
        private readonly IObtenerPromocionesService _obtenerPromociones;
        private readonly IObtenerPromocionesVigentesPorFechaService _obtenerPromocionesVigentesPorFecha;
        private readonly IObtenerPromocionesVigentesService _obtenerPromocionesVigentes;
        private readonly IObtenerPromocionService _obtenerPromocion;
        private readonly IObtenerPromocionesVigentesParaVentaService _obtenerPromocionesVigentesParaVenta;

        public PromocionController(ICrearPromocionService crearPromocion,
                                IEliminarPromocionService eliminarPromocion,
                                IModificarPromocionService modificarPromocion,
                                IModificarVigenciaService modificarVigencia,
                                IObtenerPromocionesService obtenerPromociones,
                                IObtenerPromocionesVigentesPorFechaService obtenerPromocionesVigentesPorFecha,
                                IObtenerPromocionesVigentesService obtenerPromocionesVigentes,
                                IObtenerPromocionService obtenerPromocion,
                                IObtenerPromocionesVigentesParaVentaService obtenerPromocionesVigentesParaVenta)
        {
            _crearPromocion = crearPromocion ?? throw new ArgumentNullException(nameof(crearPromocion));
            _eliminarPromocion = eliminarPromocion ?? throw new ArgumentNullException(nameof(eliminarPromocion));
            _modificarPromocion = modificarPromocion ?? throw new ArgumentNullException(nameof(modificarPromocion));
            _modificarVigencia = modificarVigencia ?? throw new ArgumentNullException(nameof(crearPromocion));
            _obtenerPromociones = obtenerPromociones ?? throw new ArgumentNullException(nameof(obtenerPromociones));
            _obtenerPromocionesVigentesPorFecha = obtenerPromocionesVigentesPorFecha ?? throw new ArgumentNullException(nameof(obtenerPromocionesVigentesPorFecha));
            _obtenerPromocionesVigentes = obtenerPromocionesVigentes ?? throw new ArgumentNullException(nameof(obtenerPromocionesVigentes));
            _obtenerPromocion = obtenerPromocion ?? throw new ArgumentNullException(nameof(obtenerPromocion));
            _obtenerPromocionesVigentesParaVenta = obtenerPromocionesVigentesParaVenta ?? throw new ArgumentNullException(nameof(obtenerPromocionesVigentesParaVenta));
        }
        [HttpGet]
        public async Task<IEnumerable<Promocion>> ObtenerPromociones()
        {
            return await _obtenerPromociones.ObtenerPromocionesAsync();
        }
        [HttpGet]
        [Route("/promocion/{id}")]
        public async Task<Promocion> ObtenerPromocion(
            Guid id)
        {
            return await _obtenerPromocion.ObtenerPromocionAsync(id);
        }
        [HttpGet]
        [Route("/promocion/vigentes")]
        public async Task<IEnumerable<Promocion>> ObtenerPromocionesVigentes()
        {
            return await _obtenerPromocionesVigentes.ObtenerPromocionesVigentesAsync();
        }
        [HttpGet]
        [Route("/promocion/vigentes/{fecha}")]
        public async Task<IEnumerable<Promocion>> ObtenerPromocionesVigentes(
            DateTime fecha)
        {
            return await _obtenerPromocionesVigentesPorFecha.ObtenerPromocionesVigentesPorFechaAsync(fecha);
        }
        [HttpPost]
        [Route("/promocion/vigentes/venta")]
        public async Task<IEnumerable<Promocion>> ObtenerPromocionesVigentesParaCompra(
            [FromBody] PromocionParaVenta p)
        {
            return await _obtenerPromocionesVigentesParaVenta.ObtenerPromocionesVigentesParaVentaAsync(p);
        }
        [HttpPut]
        public async Task ModificarPromocion(
            Promocion p)
        {
            await _modificarPromocion.ModificarPromocionAsync(p);
        }
        [HttpPost]
        [Route("/promocion/crear")]
        public async Task<Guid> CrearPromocion([FromBody] CrearPromocion p)
        {
            return await _crearPromocion.CrearPromocionAsync(p);
        }
        [HttpPut]
        [Route("/promocion/eliminar/{id}")]
        public async Task CrearPromocion(Guid id)
        {
            await _eliminarPromocion.EliminarPromocionAsync(id);
        }
        [HttpPut]
        [Route("/promocion/modificarVigencia")]
        public async Task ModificarVigencia(
            [FromBody] ModificarVigencia p)
        {
            await _modificarVigencia.ModificarVigencia(p);
        }
    }
}
