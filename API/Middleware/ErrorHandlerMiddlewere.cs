﻿using Domain.Core.Exeptions;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Net;
using Microsoft.Extensions.Logging;

namespace API.Middleware
{
    public class ErrorHandlerMiddlewere
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ErrorHandlerMiddlewere> logger;

        public ErrorHandlerMiddlewere(RequestDelegate next, ILogger<ErrorHandlerMiddlewere> _logger)
        {
            _next = next;
            logger = _logger; 
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception error)
            {
                logger.LogError("Ocurrio un error en el siguiente request:", context.Request);
                var response = context.Response;
                response.ContentType = "application/json";
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                switch (error)
                {
                    case ValidationException e:
                        response.StatusCode = (int)HttpStatusCode.BadRequest;
                        await context.Response.WriteAsync(e.Message);
                        break;
                    case Exception e:
                        response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        await context.Response.WriteAsync(e.Message);
                        break;
                }

                logger.LogError("Response:",response);

            }
        }
    }
}
