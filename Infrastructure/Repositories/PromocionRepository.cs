﻿using Domain.Core.DTOs;
using Domain.Core.Repositories;
using Infrastructure.Options;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace Infrastructure.Repositories
{
    public class PromocionRepository : IPromocionRepository
    {
        private readonly DataContext db;
        private ILogger<PromocionRepository> logger;
        private IMongoCollection<Promocion> Collection =>
     db.Collection<Promocion>(DataContext.PromocionesCollectionName);

        private IMongoQueryable<Promocion> CollectionQuery
            => db.Collection<Promocion>(DataContext.PromocionesCollectionName).AsQueryable();
        public PromocionRepository(DataContext _db, ILogger<PromocionRepository> _logger)
        {
            db = _db;
            logger = _logger;
        }

        public async Task DeleteOneAsync(Guid id)
        {
            logger.LogDebug($"Se eliminara el registro {id}");
            FilterDefinition<Promocion> filter = Builders<Promocion>.Filter.Eq(x => x.Id, id);
            UpdateDefinition<Promocion> update = Builders<Promocion>.Update.Set(x => x.Activo, false)
                                                                           .Set(x => x.FechaModificacion, DateTime.Now);
            await Collection.FindOneAndUpdateAsync(filter, update);
        }

        public async Task<Promocion> FirstOrDefaultAsync(Guid id)
        {
            logger.LogDebug($"Se buscara el registro {id}");
            return await CollectionQuery.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<Promocion>> GetAllActiveAsync()
        {
            logger.LogDebug($"Se buscaran registros activos");
            return await CollectionQuery.Where(p => p.Activo).ToListAsync();
        }

        public async Task<IEnumerable<Promocion>> GetAllActiveByDateAsync(DateTime fecha)
        {
            logger.LogDebug($"Se buscaran registros activos para la fecha {fecha}");
            return await CollectionQuery
                .Where(p => p.Activo)
                .Where(p => p.FechaInicio <= fecha && p.FechaFin >= fecha)
                .ToListAsync();
        }

        public async Task<IEnumerable<Promocion>> GetAllAsync()
        {
            logger.LogDebug($"Se buscaran todos los registros");
            return await CollectionQuery
                .ToListAsync();
        }

        public async Task<Guid> InsertOneAsync(Promocion p)
        {
            logger.LogDebug($"Se insertara el registro {p}");
            await Collection.InsertOneAsync(p);
            return p.Id;
        }

        public async Task UpdateDate(ModificarVigencia dto)
        {
            logger.LogDebug($"Se modificara el registro {dto.Id}");
            FilterDefinition<Promocion> filter = Builders<Promocion>.Filter.Eq(x => x.Id, dto.Id);
            UpdateDefinition<Promocion> update = Builders<Promocion>.Update.Set(x => x.FechaInicio, dto.FechaInicio)
                                                 .Set(x => x.FechaFin, dto.FechaFin)
                                                 .Set(x => x.FechaModificacion, DateTime.Now);
            await Collection.FindOneAndUpdateAsync(filter, update);
        }

        public async Task UpdateOneAsync(Promocion p)
        {
            logger.LogDebug($"Se modificara el registro {p.Id}");
            FilterDefinition<Promocion> filter = Builders<Promocion>.Filter.Eq(x => x.Id, p.Id);
            UpdateDefinition<Promocion> update = Builders<Promocion>.Update.Set(x => x.FechaInicio, p.FechaInicio)
                                                 .Set(x => x.FechaFin, p.FechaFin)
                                                 .Set(x => x.MaximaCantidadDeCuotas, p.MaximaCantidadDeCuotas)
                                                 .Set(x => x.Bancos, p.Bancos)
                                                 .Set(x => x.CategoriasProductos, p.CategoriasProductos)
                                                 .Set(x => x.MediosDePago, p.MediosDePago)
                                                 .Set(x => x.ValorInteresCuotas, p.ValorInteresCuotas)
                                                 .Set(x => x.FechaModificacion, DateTime.Now);
            await Collection.FindOneAndUpdateAsync(filter, update);
        }

        public async Task<IEnumerable<Promocion>> GetAllActiveForSale(PromocionParaVenta promocion)
        {
            var result = CollectionQuery.Where(x => x.Activo);

            if (promocion.Bancos!= null)
            {
                result = result.Where(p => p.Bancos.Any(x => x == promocion.Bancos));
            }
            if (promocion.MediosDePago != null)
            {
                result = result.Where(p => p.MediosDePago.Any(x => x == promocion.MediosDePago));
            }
            if (promocion.CategoriasProductos != null)
            {
                result = result.Where(p => p.CategoriasProductos.Any(x => BuscarCategoria(x, promocion.CategoriasProductos)));
            }

            return await result.ToListAsync();
        }

        private bool BuscarCategoria(string p, IEnumerable<string> CategoriasProductos)
        {
            foreach (string c in CategoriasProductos)
            {
                if (c == p) return true;
            }
            return false;
        }


    }
}
