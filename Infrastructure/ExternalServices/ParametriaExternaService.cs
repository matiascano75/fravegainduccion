﻿using Domain.Core.DTOs;
using Domain.Core.Exeptions;
using Domain.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ExternalServices
{
    public class ParametriaExternaService : IParametriaExterna
    {
        private readonly IEnumerable<string> categorias = new string[] { "Hogar", "Jardin", "ElectroCocina", "GrandesElectro", "Colchones", "Celulares", "Tecnologia", "Audio" };
        private readonly IEnumerable<string> bancos = new string[] { "Galicia", "Santander Rio", "Ciudad", "Nacion", "ICBC", "BBVA", "Macro" };
        private readonly IEnumerable<string> mediosDePago = new string[] { "TARJETA_CREDITO", "TARJETA_DEBITO", "EFECTIVO", "GIFT_CARD" };

        public void ValidarValores(Promocion p)
        {
            ValidarCategoria(p.CategoriasProductos);
            ValidarBanco(p.Bancos);
            ValidarMedioDePago(p.MediosDePago);
        }

        private void ValidarCategoria(IEnumerable<string> values)
        {
            if (values.Count() == 0) return;
            var encontro = false;
            foreach (string categoria in values)
            {
                foreach (string c in categorias)
                {
                    if (c == categoria) encontro = true;
                }
            }
            if (!encontro) throw new CategoriaNoExisteExeption();
        }
        private void ValidarBanco(IEnumerable<string> values)
        {
            if (values.Count() == 0) return;
            var encontro = false;
            foreach (string banco in values)
            {
                foreach (string c in bancos)
                {
                    if (c == banco) encontro = true;
                }
            }
            if (!encontro) throw new BancoNoExisteExeption();
        }
        private void ValidarMedioDePago(IEnumerable<string> values)
        {
            if (values.Count() == 0) return;
            var encontro = false;
            foreach (string medioDePago in values)
            {
                foreach (string c in mediosDePago)
                {
                    if (c == medioDePago) encontro = true;
                }
            }
            if (!encontro) throw new MedioDePagoNoExisteExeption();
        }
    }
}
