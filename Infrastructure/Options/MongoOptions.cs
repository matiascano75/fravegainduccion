﻿namespace Infrastructure.Options
{
    public class MongoOptions 
    {
        public string DatabaseName { get; set; }
        public string ConnectionString { get; set; }
        public string Collection { get; set; }
    }
}
