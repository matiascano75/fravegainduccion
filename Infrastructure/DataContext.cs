﻿using Infrastructure.Options;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Infrastructure
{
    public class DataContext
    {
        public IMongoDatabase MongoDatabase { get; set; }

        public static string PromocionesCollectionName => "Promociones";
     
        public DataContext()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            MongoDatabase = client.GetDatabase("InduccionFravega");
        }
        public IMongoCollection<TEntity> Collection<TEntity>(string name) => MongoDatabase.GetCollection<TEntity>(name);
    }
}
