﻿using Domain.Core.Validators;
using FluentAssertions;
using System;
using Test.Utils;
using Xunit;
using Domain.Core.Tests.Services.Customization;
using Domain.Core.DTOs;
using Domain.Core.Exeptions;
using AutoFixture.Xunit2;
using Domain.Core.Repositories;
using Moq;

namespace Domain.Core.Tests.ValidatorsTests
{
    public class ValidarPromocionTests
    {
        [Theory]
        [DefaultData(typeof(CantidadCuotasOPorcentajeDescuentoSinValorTestCase))]

        public void CantidadCuotasOPorcentajeDescuentoSinValor_ShouldThrowExeption(
            Promocion nuevaPromocion,
            [Frozen] Mock<IPromocionRepository> _promocionRepository)
        {
            //arrange
            ValidarPromocion sut = new ValidarPromocion(_promocionRepository.Object);
            //act
            Action action = () =>
            {
                sut.CantidadCuotasOPorcentajeDescuentoSinValor(nuevaPromocion);
            };
            //assert
            action.Should().Throw<CantCuotasOProcentajeDescSinValorExeption>();
        }

        [Theory]
        [DefaultData(typeof(PorcentajeInteresSinCantCuotasTestCase))]
        public void PorcentajeInteresSinCantCuotas_ShouldThrowExeption(
            [Frozen] Mock<IPromocionRepository> _promocionRepository,
            Promocion nuevaPromocion)
        {
            //arrange
            ValidarPromocion sut = new ValidarPromocion(_promocionRepository.Object);
            nuevaPromocion.PorcentajeDeDescuento = 50;
            nuevaPromocion.MaximaCantidadDeCuotas = null;
            //act
            Action action = () =>
            {
                sut.PorcentajeInteresSinCantCuotas(nuevaPromocion);
            };
            //assert
            action.Should().Throw<PorcentajeInteresSinCantCuotasExeption>();
        }

        [Theory]
        [DefaultData(typeof(FechaFinYFechaInicioInvalidasTestCase))]
        public void FechaFinYFechaInicioInvalidas_ShouldThrowExeption(
            [Frozen] Mock<IPromocionRepository> _promocionRepository,
            Promocion p
        )
        {
            //arrange
            ValidarPromocion sut = new ValidarPromocion(_promocionRepository.Object);

            //act
            Action action = () =>
            {
                sut.FechaFinYFechaInicioInvalidas(p.FechaInicio, p.FechaFin);
            };
            //assert
            action.Should().Throw<FechaFinYFechaInicioInvalidasExeption>();
        }

        [Theory]
        [DefaultData(typeof(PorcentajeDeDescuentoInvalidTestCase))]
        public void PorcentajeDeDescuentoInvalido_ShouldThrowExeption(
            Promocion nuevaPromocion,
            [Frozen] Mock<IPromocionRepository> _promocionRepository
            )
        {
            //arrange
            ValidarPromocion sut = new ValidarPromocion(_promocionRepository.Object);
            //act
            Action action = () =>
            {
                sut.PorcentajeDeDescuentoInvalido(nuevaPromocion);
            };
            //assert
            action.Should().Throw<DescuentoInvalidoExeption>();
        }
    }
}
