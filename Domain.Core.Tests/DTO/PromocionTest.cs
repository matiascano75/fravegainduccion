﻿using Domain.Core.DTOs;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Utils;
using Xunit;

namespace Domain.Core.Tests.DTO
{
    public class PromocionTest
    {
        [DefaultData]
        [Theory]
        public void DtoToEntity_Should_AssingProperly(Promocion sut)
        {
            //arrange
            Domain.Entities.Promocion p;
            //act
            p = sut.CreateEntity();
            //assert
            p.MaximaCantidadDeCuotas.Should().Be(sut.MaximaCantidadDeCuotas);
            p.ValorInteresCuotas.Should().Be(sut.ValorInteresCuotas);
            p.PorcentajeDeDescuento.Should().Be(sut.PorcentajeDeDescuento);
            p.FechaInicio.Should().Be(sut.FechaInicio);
            p.FechaFin.Should().Be(sut.FechaFin);
            p.MediosDePago.Should().BeEquivalentTo(sut.MediosDePago);
            p.Bancos.Should().BeEquivalentTo(sut.Bancos);
            p.PorcentajeDeDescuento.Should().Be(sut.PorcentajeDeDescuento);
            p.CategoriasProductos.Should().BeEquivalentTo(sut.CategoriasProductos);
            p.Id.Should().Be(sut.Id.ToString());                
        }
    }
}
