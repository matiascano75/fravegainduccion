﻿using Domain.Core.DTOs;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Utils;
using Xunit;

namespace Domain.Core.Tests.DTO
{
    public class CrearPromocionTest
    {
        [Theory]
        [DefaultData]
        public void CrearPromocionCreateEntity_Should_ReturnEntity(CrearPromocion p)
        {
            //arrange
            //act
            var result = p.CreateDto();
            //assert
            result.Should().BeOfType<Promocion>();
            result.MediosDePago.Should().Equal(p.MediosDePago);
            result.Bancos.Should().Equal(p.Bancos);
            result.CategoriasProductos.Should().Equal(p.CategoriasProductos);
            result.MaximaCantidadDeCuotas.Should().Be(p.MaximaCantidadDeCuotas);
            result.PorcentajeDeDescuento.Should().Be(p.PorcentajeDeDescuento);
            result.Id.Should().NotBeEmpty();
            result.Activo.Should().Be(true);
            result.FechaInicio.Should().Be(p.FechaInicio);
            result.FechaFin.Should().Be(p.FechaFin);
        }
    }
}
