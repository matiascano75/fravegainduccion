﻿using AutoFixture;
using Domain.Core.DTOs;
using System;

namespace Domain.Core.Tests.Services.Customization
{
    public abstract class BasePromocionCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            //var channel = new Promocion(3, 5, 10, null, null, new string[] { "Tarjeta_debito" }, new string[] { "Galicia" }, new string[] { "Heladera" }, null); 

            var promocion = CrearPromocion(fixture);
            fixture.Inject(promocion);
        }
        protected abstract Promocion CrearPromocion(IFixture fixture);
    }

    public class CantidadCuotasOPorcentajeDescuentoSinValorTestCase : BasePromocionCustomization
    {
        protected override Promocion CrearPromocion(IFixture fixture)
        {
            return new Promocion(new string[] { "TARJETA_CREDITO" }, new string[] { "Galicia" }, new string[] { "Electrodomestico" }, null, null, null, null, null, null,null);
        }
    }

    public class PromocionValidaTestCase : BasePromocionCustomization
    {
        protected override Promocion CrearPromocion(IFixture fixture)
        {
            return new Promocion(new string[] { "TARJETA_CREDITO" }, new string[] { "Galicia" }, new string[] { "Electrodomestico" }, 6, 4, 1, null, null, null, null);
        }
    }
    public class PorcentajeInteresSinCantCuotasTestCase : BasePromocionCustomization
    {
        protected override Promocion CrearPromocion(IFixture fixture)
        {
            return new Promocion(new string[] { "TARJETA_CREDITO" }, new string[] { "Galicia" }, new string[] { "Electrodomestico" }, null, 4, 1, null, null, null, null);
        }
    }
    public class FechaFinYFechaInicioInvalidasTestCase : BasePromocionCustomization
    {
        protected override Promocion CrearPromocion(IFixture fixture)
        {
            return new Promocion(new string[] { "TARJETA_CREDITO" }, new string[] { "Galicia" }, new string[] { "Electrodomestico" }, 6, 4, 24, new DateTime(2017, 5, 28), new DateTime(2016, 5, 28), null, null);
        }
    }
    public class PorcentajeDeDescuentoInvalidTestCase : BasePromocionCustomization
    {
        protected override Promocion CrearPromocion(IFixture fixture)
        {
            return new Promocion(new string[] { "TARJETA_CREDITO" }, new string[] { "Galicia" }, new string[] { "Electrodomestico" }, 6, 4, 1, null, null, null, null);
        }
    }


}
