﻿using AutoFixture;
using Domain.Core.DTOs;
using System;
using System.Collections.Generic;

namespace Domain.Core.Tests.Services.Customization
{
    public abstract class BasePromocionListCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            //var channel = new Promocion(3, 5, 10, null, null, new string[] { "Tarjeta_debito" }, new string[] { "Galicia" }, new string[] { "Heladera" }, null); 

            var promocion = CrearListaPromocion(fixture);
            fixture.Inject(promocion);
        }
        protected abstract IEnumerable<Promocion> CrearListaPromocion(IFixture fixture);
    }

    public class ListaPromocionesTestCase : BasePromocionListCustomization
    {
        protected override IEnumerable<Promocion> CrearListaPromocion(IFixture fixture)
        {
            List<Promocion> promociones = new List<Promocion>();
            promociones.Add(new Promocion(new string[] { "TARJETA_CREDITO" }, new string[] { "Galicia" }, new string[] { "Electrodomestico" }, 6, 4, 24, null, null, null, null));
            promociones.Add(new Promocion(new string[] { "TARJETA_DEBITO" }, new string[] { "Galicia" }, new string[] { "Electrodomestico" }, 6, 4, 24, null, null, null, null));
            promociones.Add(new Promocion(new string[] { "TARJETA_DEBITO" }, new string[] { "Galicia" }, new string[] { "Electrodomestico" }, 6, 4, 24, null, null, null, null));

            return promociones;
        }
    }

    public class ListaPromocionesActivasTestCase : BasePromocionListCustomization
    {
        protected override IEnumerable<Promocion> CrearListaPromocion(IFixture fixture)
        {
            List<Promocion> promociones = new List<Promocion>();
            promociones.Add(new Promocion(new string[] { "credito" }, new string[] { "Galicia" }, new string[] { "Electrodomestico" }, 6, 4, 24, null, null, null, true));
            promociones.Add(new Promocion(new string[] { "credito" }, new string[] { "Galicia" }, new string[] { "Electrodomestico" }, 6, 4, 24, null, null, null, false));
            return promociones;
        }
    }

}
