﻿using AutoFixture.Xunit2;
using Domain.Core.DTOs;
using Domain.Core.Repositories;
using Domain.Core.Services;
using Domain.Core.Tests.Services.Customization;
using Domain.Core.Validators;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Test.Utils;
using Xunit;

namespace Domain.Core.Tests.Services
{
    public class ObtenerPromocionesVigentesPorFechaTest
    {
        [Theory]
        [DefaultData(typeof(ListaPromocionesTestCase))]
        public async void ObtenerPromocionesVigentesPorFecha_Should_ReturnOK(
            [Frozen] Mock<IPromocionRepository> _promocionRepository,
            [Frozen] Mock<IValidarPromocion> validarPromocionMock,
            List<Promocion> promociones,
            DateTime date
        )
        {
            //arrange
            //_promocionRepository.Setup(x => x.GetAllActiveAsync()).ReturnsAsync(promociones);
            _promocionRepository.Setup(x => x.GetAllActiveByDateAsync(It.IsAny<DateTime>())).ReturnsAsync(promociones);
            ObtenerPromocionesVigentesPorFechaService sut = new ObtenerPromocionesVigentesPorFechaService(_promocionRepository.Object, validarPromocionMock.Object);

            //act

            Func<Task> result = async () => await sut.ObtenerPromocionesVigentesPorFechaAsync(date);

            //assert
            result.Should().NotBeNull();
        }
    }
}
