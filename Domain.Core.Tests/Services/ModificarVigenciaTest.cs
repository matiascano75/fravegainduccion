﻿using AutoFixture.Xunit2;
using Domain.Core.DTOs;
using Domain.Core.Repositories;
using Domain.Core.Services;
using Domain.Core.Tests.Services.Customization;
using Domain.Core.Validators;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Utils;
using Xunit;

namespace Domain.Core.Tests.Services
{
    public class ModificarVigenciaTest
    {
        [Theory]
        [DefaultData(typeof(PromocionValidaTestCase))]
        public async void ModificarVigenciaTest_Should_ReturnOK(
    [Frozen] Mock<IPromocionRepository> _promocionRepository,
    [Frozen] Mock<IValidarPromocion> _validarPromocion,
    ModificarVigencia promocion)
        {
            //arrange
            _promocionRepository.Setup(x => x.UpdateDate(promocion)).Returns(Task.CompletedTask);
            ModificarVigenciaService sut = new ModificarVigenciaService(_promocionRepository.Object,_validarPromocion.Object);

            //act
            Func<Task> result = async () => await sut.ModificarVigencia(promocion);

            //assert
            result.Should().BeOfType<Func<Task>>();
        }
    }
}
