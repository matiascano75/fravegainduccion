﻿using AutoFixture.Xunit2;
using Domain.Core.DTOs;
using Domain.Core.Repositories;
using Domain.Core.Services;
using Domain.Core.Tests.Services.Customization;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Test.Utils;
using Xunit;

namespace Domain.Core.Tests.Services
{
    public class ObtenerPromocionesTest
    {
        [Theory]
        [DefaultData(typeof(ListaPromocionesTestCase))]
        public async void ObtenerPromocionesTest_Should_ReturnOK(
        [Frozen] Mock<IPromocionRepository> _promocionRepository,
        List<Promocion> promociones)
        {
            //arrange
            _promocionRepository.Setup(x => x.GetAllAsync()).ReturnsAsync(promociones);
            ObtenerPromocionesService sut = new ObtenerPromocionesService(_promocionRepository.Object);

            //act
            Func<Task> result = async () => await sut.ObtenerPromocionesAsync();

            //assert
            result.Should().NotBeNull();
        }
    }
}
