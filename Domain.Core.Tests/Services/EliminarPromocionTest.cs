﻿using AutoFixture.Xunit2;
using Domain.Core.DTOs;
using Domain.Core.Repositories;
using Domain.Core.Services;
using FluentAssertions;
using Moq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test.Utils;
using Xunit;

namespace Domain.Core.Tests.Services.Customization
{
    public class EliminarPromocionTest
    {
        [Theory]
        [DefaultData]
        public async void EliminarPromocionTest_Should_ReturnOK(
            [Frozen] Mock<IPromocionRepository> _promocionRepository
            )
        {
            //arrange
            _promocionRepository.Setup(x => x.DeleteOneAsync(It.IsAny<Guid>())).Returns(Task.CompletedTask);
            //Guid guid = lista.FirstOrDefault().Id;
            EliminarPromocionService sut = new EliminarPromocionService(_promocionRepository.Object);

            //act
            Func<Task> result = async () => await sut.EliminarPromocionAsync(Guid.NewGuid());

            //assert
            result.Should().BeOfType<Func<Task>>();
        }
    }
}

