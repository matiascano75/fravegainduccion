﻿using AutoFixture.Xunit2;
using Domain.Core.DTOs;
using Domain.Core.Repositories;
using Domain.Core.Services;
using Domain.Core.Tests.Services.Customization;
using Domain.Core.Validators;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Utils;
using Xunit;

namespace Domain.Core.Tests.Services
{
    public class ModificarPromocionTest
    {
        [Theory]
        [DefaultData(typeof(PromocionValidaTestCase))]
        public async void ModificarPromocionTest_Should_ReturnOK(
    [Frozen] Mock<IPromocionRepository> _promocionRepository,
    [Frozen] Mock<IValidarPromocion> _validarPromocion,
    [Frozen] Mock<IParametriaExterna> parametriaExternaMock,
    Promocion promocion)
        {
            //arrange
            _promocionRepository.Setup(x => x.UpdateOneAsync(promocion)).Returns(Task.CompletedTask);
            //Guid guid = lista.FirstOrDefault().Id;
            ModificarPromocionService sut = new ModificarPromocionService(_promocionRepository.Object, _validarPromocion.Object,parametriaExternaMock.Object);

            //act
            Func<Task> result = async () => await sut.ModificarPromocionAsync(promocion);

            //assert
            result.Should().BeOfType<Func<Task>>();
        }
    }
}
