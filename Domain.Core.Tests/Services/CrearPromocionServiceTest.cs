﻿using AutoFixture.Xunit2;
using Domain.Core.DTOs;
using Domain.Core.Repositories;
using Domain.Core.Services;
using Domain.Core.Tests.Services.Customization;
using Domain.Core.Validators;
using FluentAssertions;
using Moq;
using System;
using Test.Utils;
using Xunit;

namespace Domain.Core.Tests.Services
{
    public class CrearPromocionServiceTest
    {
        [Theory]
        [DefaultData(typeof(PromocionValidaTestCase))]
        public async void CrearPromocionService_Should_ReturnGuid(
            CrearPromocion p,
            [Frozen] Mock<IPromocionRepository> repositoryMock,
            [Frozen] Mock<IValidarPromocion> validarPromocionMock,
            [Frozen] Mock<IParametriaExterna> parametriaExternaMock)
        {
            //arrange
            repositoryMock.Setup(p => p.InsertOneAsync(It.IsAny<Promocion>())).ReturnsAsync(new Guid());
            var sut = new CrearPromocionService(repositoryMock.Object, validarPromocionMock.Object, parametriaExternaMock.Object);
            //act 
            var result = await sut.CrearPromocionAsync(p);
            //assert
            sut.Should().NotBeNull();
        }
    }
}
