﻿using AutoFixture.Xunit2;
using Domain.Core.DTOs;
using Domain.Core.Repositories;
using Domain.Core.Services;
using Domain.Core.Tests.Services.Customization;
using Domain.Core.Validators;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Test.Utils;
using Xunit;

namespace Domain.Core.Tests.Services
{
    public class ObtenerPromocionesVigentesPorVentaTest
    {
        [Theory]
        [DefaultData(typeof(ListaPromocionesTestCase))]
        public async void ObtenerPromocionesVigentesPorVenta_Should_ReturnOK(
            [Frozen] Mock<IPromocionRepository> _promocionRepository,
            List<Promocion> promociones,
            PromocionParaVenta p
        )
        {
            //arrange
            //_promocionRepository.Setup(x => x.GetAllActiveAsync()).ReturnsAsync(promociones);
            _promocionRepository.Setup(x => x.GetAllActiveForSale(It.IsAny<PromocionParaVenta>())).ReturnsAsync(promociones);
            ObtenerPromocionesVigentesParaVentaService sut = new ObtenerPromocionesVigentesParaVentaService(_promocionRepository.Object);

            //act
            Func<Task> result = async () => await sut.ObtenerPromocionesVigentesParaVentaAsync(p);

            //assert
            result.Should().NotBeNull();
        }
    }
}
