﻿using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Xunit2;
using Domain.Core.DTOs;
using Domain.Core.Repositories;
using Domain.Core.Services;
using Domain.Core.Tests.Services.Customization;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Test.Utils;
using Xunit;

namespace Domain.Core.Tests.Services
{
    public class ObtenerPromocionTest
    {
        [Theory]
        [DefaultData(typeof(PromocionValidaTestCase))]
        public async void ObtenerPromocionTest_Should_ReturnOK(
        [Frozen] Mock<IPromocionRepository> _promocionRepository)
        { 
             _promocionRepository.Setup(x => x.FirstOrDefaultAsync(It.IsAny<Guid>())).ReturnsUsingFixture(new Fixture());
            ObtenerPromocionService sut = new ObtenerPromocionService(_promocionRepository.Object);

            //act
            Func<Task> result = async () => await sut.ObtenerPromocionAsync(Guid.NewGuid());

            //assert
            result.Should().NotBeNull();
        }
    }
}
