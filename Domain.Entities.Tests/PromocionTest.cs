using Domain.Entities;
using FluentAssertions;
using System;
using Test.Utils;
using Xunit;

namespace Domain.Entities.Tests
{
    public class PromocionTest
    {
        /*
        nombre del metodo de test: {nombre del metodo}_{precondicion}_{resultado esperado}
        ej: Const_Valid_shouldWork
            Const_BankInvalid_ShouldThrowBankInvalidException
        
        cuerpo del metodo: AAA

        ej:

        public void Const_Valid_shouldWork(...)
        {
            //arrange
        --> todas las lineas de codigo que necesito para ejecutar el test
            //act
        --> llamada al sut (system under test)
            //assert
        --> asserts
        }
        */

        [Theory]
        [DefaultData]
        public void Const_Valid_shouldWork(int maximaCantidadDeCuotas, decimal valorInteresCuotas,
            DateTime fechaInicio, DateTime fechaFin)
        {
            //arrange
            var bancos = new string[] { "Galicia" };
            var categoriaProductos = new string[] { "Hogar" };
            var mediosDePago = new string[] { "TARJETA_CREDITO" };

            //act
            var result = new Promocion(
                maximaCantidadDeCuotas, valorInteresCuotas, 0,
                fechaInicio, fechaFin, mediosDePago, bancos, categoriaProductos, Guid.NewGuid());

            //assert
            result.MaximaCantidadDeCuotas.Should().Be(maximaCantidadDeCuotas);
            result.ValorInteresCuotas.Should().Be(valorInteresCuotas);
            result.FechaInicio.Should().Be(fechaInicio);
            result.FechaFin.Should().Be(fechaFin);
            result.Bancos.Should().BeEquivalentTo(bancos);
            result.CategoriasProductos.Should().BeEquivalentTo(categoriaProductos);
            result.MediosDePago.Should().BeEquivalentTo(mediosDePago);
            result.Activo.Should().BeTrue();
        }
        /*
        [Theory]
        [DefaultData]
        public void Const_BothPromotion_shouldFail(decimal valorInteresCuotas,
            DateTime fechaInicio, DateTime fechaFin, string[] mediosDePago,
            string[] bancos, string[] categoriasProductos,
            int cantidadCuotas)
        {
            //arrange
            int maximaCantidadDeCuotas = 20;
            decimal porcentajeDescuento = 5;

            //act
            /*var result = new Promocion(
            maximaCantidadDeCuotas, valorInteresCuotas, porcentajeDescuento,
            fechaInicio, fechaFin, mediosDePago, bancos, categoriasProductos);

        Action comparison = () =>
        {
            new Promocion(
            maximaCantidadDeCuotas, valorInteresCuotas, porcentajeDescuento,
             fechaInicio, fechaFin, mediosDePago, bancos, categoriasProductos);
        };
        //assert

        comparison.Should().Throw<Exception>();
        }  
        */
    }
}